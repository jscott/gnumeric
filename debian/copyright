Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnumeric
Source: http://ftp.gnome.org/pub/GNOME/sources/gnumeric/
Comment:
 This package was originally debianized by Vincent Renardias <vincent@waw.com>
 on Sat, 26 Sep 1998 17:15:24 +0200.

Files: *
Copyright:
    2010-2017 by The Gnumeric Team and others
    2001-2016 Andreas J. Guelzow <aguelzow@pyrshep.ca>, <aguelzow@taliesin.ca>
    2001      Almer. S. Tigelaar <almer@gnome.org>, <almer1@dds.nl>, <almer-t@bigfoot.com>
    2001      Ariya Hidayat <ariyahidayat@yahoo.de>
    1997-1998 Andrew Tridgell
    1996-2000 Brian Gough
    1998      Chris Lahey
    1998      David Abilleira Freijeiro <odaf@nexo.es>
    2013      Dmitry Matveev
    1995-1997,1999-2001,2005-2006 Free Software Foundation, Inc.
    2000-2001 Frodo Looijaard <frodol@dds.nl>
    1998-2002 GNOME Foundation
    2002-2003 Ian Smith
    2005      INdT - Instituto Nokia de Tecnologia
    2007-2015 Jean Bréfort <jean.brefort@normalesup.org>
    1999      Jeff Garzik <jgarzik@mandrakesoft.com>
    1998-2014 Jody Goldberg <jody@gnome.org>
    1998-2014 Jon K Hellan <hellan@acm.org>
    1996-2000 Jorma Olavi T.htinen
    2000      JP Rosevear <jpr@arcavia.com>
    2000-2003,2005 Jukka-Pekka Iivonen <iivonen@iki.fi>, <jiivonen@hutcs.cs.hut.fi>
    2006      Laurency Franck
    2006      Luciano Miguel Wolf <luciano.wolf@indt.org.br>
    1998,1999 Michael Lausch
    1998-2014 Michael Meeks <miguel@kernel.org>, <michael@ximian.com>
    1997-2005 Miguel de Icaza <miguel@novell.com>, <miguel@gnu.org>, <miguel@kernel.org>
    1998-2018 Morten Welinder <terra@gnone.org>
    1999,2000 Rasca, Berlin <thron@gmx.de>
    2000-2002 Rodrigo Moya
    2006      Vivien Malerba
    2000-2001 Ximian, Inc.
License: GPL-2 or GPL-3
Comment:
 Note that these are the license terms for the Gnumeric application as a
 whole.
 .
 Some of the source files for this application are licensed under
 different terms which are compatible with the GNU General Public License
 version 2, e.g. terms that allow relicensing under later versions of that
 license.
 .
 Some files have GPL-2+ license headers.

Files: src/mathfunc.c
Copyright: 1998      Ross Ihaka
           2000-2007 The R Core Team
           1999-2001 The R Development Core Team
           2003-2005 The R Foundation
           2002-2003 Ian Smith
           2004-2005 Morten Welinder <terra@gnome.org>
           1995-1996 Robert Gentleman and Ross Ihaka
           1997-1988 Royal Statistical Society
License: GPL-2+

Files: tools/dumpdef.pl
       plugins/fn-financial/sc-fin.c
Copyright: 2005 Ivan, Wong Yat Cheung <email@ivanwong.info>
           2000 Sun Microsystems, Inc
License: LGPL-2.1

Files:
    m4/ltoptions.m4
    m4/ltsugar.m4
    m4/ltversion.m4
    m4/lt~obsolete.m4
Copyright:
    2004-2005,2007-2009,2011-2015 Free Software Foundation, Inc
License: FSFULLR

Files:
    m4/ax_is_release.m4
Copyright:
    2015      Philip Withnall <philip@tecnocode.co.uk>
License: FSF-Note

Files:
    m4/libtool.m4
Copyright:
    1996-2001,2003-2015 Free Software Foundation, Inc
License: FSFULLR and GPL-2+ and exception-GPL-Libtool

Files:
    m4/nls.m4
Copyright:
    1995-2003,2005-2006,2008-2014 Free Software Foundation, Inc
License: GPL-2+

Files:
    m4/intltool.m4
    m4/pkg.m4
Copyright:
    2012-2015 Dan Nicholson <dbn.lists@gmail.com>
    2001      Eazel, Inc
    2004      Scott James Remnant <scott@netsplit.com>
License: GPL-2+

Files:
    m4/glib-gettext.m4
Copyright:
    1995-2002 Free Software Foundation, Inc
    2001-2004 Red Hat, Inc
License: GPL-2+ and exception-GPL-Autoconf

Files: debian/*
Copyright:
    2013-2023 Dmitry Smirnov <onlyjob@debian.org>
    2001-2013 J.H.M. Dassen (Ray) <jdassen@debian.org>
    1998-2001 Vincent Renardias <vincent@debian.org>
License: GPL-2+

License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it
 under the terms of version 2.1 of the GNU Lesser General Public License
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 The complete text of the GNU Lesser General Public License
 can be found in the file "/usr/share/common-licenses/LGPL-2.1".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License 3
 can be found in "/usr/share/common-licenses/GPL-3".

License: FSFULLR
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

License: exception-GPL-Libtool
 As a special exception to the GNU General Public License,
 if you distribute this file as part of a program or library that
 is built using GNU Libtool, you may include this file under the
 same distribution terms that you use for the rest of that program.

License: FSF-Note
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.

License: exception-GPL-Autoconf
 As a special exception to the GNU General Public License, this file may be
 distributed as part of a program that contains a configuration script
 generated by Autoconf, under the same distribution terms as the rest of
 that program.
